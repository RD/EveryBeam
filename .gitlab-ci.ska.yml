# Copyright (C) 2021 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: GPL-3.0-or-later

# This file contains the pipelines that run on the SKAO repository (mirror) of
# EveryBeam, which is at https://gitlab.com/ska-telescope/sdp/ska-sdp-func-everybeam

.dind-requester:
  services:
    - docker:20.10-dind

# At SKAO, the cibuild-python-wheels job needs this custom configuration.
.cibuildwheels-custom:
  extends: .dind-requester
  tags:
    - ska-default

include:
  - local: .gitlab-ci.common.yml
# Create Gitlab CI badges from CI metrics
# https://developer.skao.int/en/latest/tools/continuousintegration.html#automated-collection-of-ci-health-metrics-as-part-of-the-ci-pipeline
# 20250203: Updated to gitlab-ci/includes/finaliser.gitlab-ci.yml, because
# gitlab-ci/includes/post_step.yml gives a warning it's deprecated.
  - project: ska-telescope/templates-repository
    file: gitlab-ci/includes/finaliser.gitlab-ci.yml

# Caching 'public' allows keeping the 'pages' output of multiple branches / MRs.
cache:
  paths:
    - public

pages:
  stage: deploy
  needs: ["versioning","test-and-coverage-2204","build-doc-2204"]
  image: $BASE_IMAGE_2204
  variables:
    OUTPUT: public/$CI_COMMIT_REF_SLUG
  script:
    - echo Deploying GitLab pages to $CI_PAGES_URL/$CI_COMMIT_REF_SLUG
    - mkdir -p $OUTPUT/coverage
    - gcovr -j`nproc` -a build/coverage.json --html-details $OUTPUT/coverage/index.html
    - cp -a doc/pages-index.html $OUTPUT/index.html
    - cp -a build/doc/html $OUTPUT/doc
  artifacts:
    name: $CI_COMMIT_REF_SLUG
    paths:
      - public
    expire_in: 1 week
