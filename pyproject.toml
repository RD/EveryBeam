#########################
# build-system settings #
#########################

[build-system]
requires = [
    "scikit-build-core",
]
build-backend = "scikit_build_core.build"


####################
# project settings #
####################

[project]
name = "everybeam"
version = "0.7.1"  # Keep in sync with top-level `CMakeLists.txt` file
description = "EveryBeam"
readme = {file = "README.md", content-type = "text/markdown"}
requires-python = ">=3.7"
license = {text = "GPLv3+"}
authors = [
    {name = "Bram Veenboer", email =  "veenboer@astron.nl"},
    {name = "André Offringa", email = "offringa@astron.nl"},
    {name = "Sebastiaan van der Tol", email = "tol@astron.nl"},
    {name = "Tammo Jan Dijkema", email = "dijkema@astron.nl"},
    {name = "Jakob Maljaars", email = "jakob.maljaars@stcorp.nl"},
    {name = "Maik Nijhuis", email = "maik.nijhuis@triopsys.nl"},
]
classifiers = [
    "Development Status :: 5 - Production/Stable",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
    "Programming Language :: C++",
    "Programming Language :: Python :: 3",
    "Topic :: Scientific/Engineering :: Astronomy",
]
dependencies = [
    "numpy",
]

[project.urls]
Homepage = "https://git.astron.nl/RD/EveryBeam/"
Documentation = "https://everybeam.readthedocs.io/"
Repository = "https://git.astron.nl/RD/EveryBeam.git"


#########################
# cibuildwheel settings #
#########################

[tool.cibuildwheel]
## Build
before-all = "cibuildwheel/before_all.sh"
before-build = "cibuildwheel/before_build.sh"
build = "cp3{8,9,10,11,12,13}-*_x86_64"
# before_all.sh installs Boost at a custom path.
environment = """ \
    WORKDIR="/tmp" \
    CASACORE_VERSION="3.6.0" \
    HDF5_VERSION="1.12.2" \
    BOOST_INCLUDEDIR=/usr/include/boost169 \
    BOOST_LIBRARYDIR=/usr/lib64/boost169 \
"""
## Test
before-test = [
    "scripts/download_ms.sh lba.MS.tar.bz2 LOFAR_LBA_MOCK.ms",
    "scripts/download_ms.sh L258627-one-timestep.tar.bz2 LOFAR_HBA_MOCK.ms",
]
test-command = [
    "export DATA_DIR={project}",
    "export EVERYBEAM_DATADIR=$(dirname $(which python))/../share/everybeam",
    "pytest -v {package}/python/test/test_pybindings.py"
]
test-requires = [ "pytest" ]

[tool.cibuildwheel.macos]
repair-wheel-command = """\
    DYLD_LIBRARY_PATH=${BOOST_INSTALL_DIR}/lib delocate-wheel \
    --require-archs {delocate_archs} -w {dest_dir} -v {wheel}\
"""

[tool.cibuildwheel.config-settings]
"cmake.define.CMAKE_CXX_FLAGS" = "-Dcasacore=everybeam::casacore"

[tool.cibuildwheel.linux]
skip = ["*-musllinux_*"]

#########################
# scikit-build settings #
#########################

[tool.scikit-build]
cmake.version = ">=3.15"
ninja.version = ">=1.5"
install.components = ["data-files", "libraries"]
logging.level = "INFO"

[tool.scikit-build.cmake.define]
BUILD_WITH_PYTHON = "ON"
